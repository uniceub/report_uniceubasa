<?php

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * classes/task/cron_task.php
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

namespace report_uniceubasa\task;

class cron_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('crontask', 'report_uniceubasa');
    }

    /**
     * Run uniceubasa cron.
     */
    public function execute() {
        global $CFG;
        require_once($CFG->dirroot . '/report/uniceubasa/lib.php');
        uniceubasa_cron();
    }

}