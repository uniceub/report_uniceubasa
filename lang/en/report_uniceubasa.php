<?php

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * lang/en/report_uniceubasa.php
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

$string['ai'] = 'Avaliação Institucional';
$string['av1'] = '1ª Avaliação';
$string['av2'] = '2ª Avaliação';
$string['av2_ai'] = '2ª Avaliação com Avaliação Institucional';
$string['avu'] = 'Única Avaliação';
$string['avu_ai'] = 'Única Avaliação com Avaliação Institucional';
$string['courses_filter'] = "Filtro de Cursos";
$string['crontask'] = 'Atualiza as senhas a cada 15 minutos e controla a exibição das abas de Conteúdo';
$string['dateend'] = "Data/Hora Final";
$string['datestart'] = "Data/Hora Inicial";
$string['messageprovider:report'] = 'Relatório UniCEUB';
$string['minlength_three_char'] = "Por favor, informe no mínimo três caracteres para pesquisa.";
$string['notdefined'] = "Não definido";
$string['nopermissions'] = "Você não tem permissão para visualizar esta página.";
$string['noresult'] = "Nenhum registro encontrado.";
$string['pluginname'] = "UniCEUB - Automatização de Senhas para Avaliações";
$string['pluginnamemin'] = "Automatização de Senhas para Avaliações";
$string['result_search'] = "Resultado da Busca";
$string['iprestriction'] = "Restrição de IP";
$string['settings_interval'] = "Intervalo para atualização da senha";
$string['time_type'] = "Horário e Tipo de Avaliação";
$string['timemodified'] = "Última Modificação";
$string['type_test'] = "Tipo de Avaliação";
$string['update'] = "Atualizado em";