<?php

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * index_form.php
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

require($CFG->libdir.'/formslib.php');
require_once($CFG->libdir. '/coursecatlib.php');

class filter_course_form extends moodleform {

	function definition() {
		global $param;

		$mform    =& $this->_form;
		$renderer =& $mform->defaultRenderer();

		$mform->addElement('header', '', get_string('courses_filter', 'report_uniceubasa'));

		$mform->addElement('text', 'name', get_string('name'),'size="30"');
		$mform->setType('name', PARAM_TEXT);
		$mform->setDefault('name', $param->name);
		$mform->addRule('name', get_string('minlength_three_char','report_uniceubasa'), 'minlength', 3, 'client', false, true);

		$categories[0] = get_string('all');
		$categories_list = coursecat::make_categories_list();
		foreach ($categories_list as $key => $value)
			$categories[$key] = $value;
		
		$mform->addElement('select', 'category', get_string('category'), $categories);
		$mform->setType('category', PARAM_TEXT);
		$mform->setDefault('category', $param->category);

		$buttonarray=array();
		$buttonarray[] = &$mform->createElement('submit', 'search_submit', get_string('search'));
		$buttonarray[] = &$mform->createElement('submit', 'password_submit', get_string('password'));
		$buttonarray[] = &$mform->createElement('cancel');
		$mform->addGroup($buttonarray, 'buttonar', '', array(''), false);
	}

	function validation($data, $files) {
		$errors = parent::validation($data, $files);
		return $errors;
	}

}


class time_type_form extends moodleform {

	function definition() {
		global $param;

		$mform    =& $this->_form;
		$renderer =& $mform->defaultRenderer();
		
		$date = usergetdate(time());
		$midnight = make_timestamp($date['year'], $date['mon'], $date['mday']);

		$mform->addElement('header', '', get_string('time_type', 'report_uniceubasa'));

		$mform->addElement('date_time_selector', 'datestart', get_string('datestart', 'report_uniceubasa'), array('required' => true, 'defaulttime' => $midnight, 'optional' => ''));

		$mform->addElement('date_time_selector', 'dateend', get_string('dateend', 'report_uniceubasa'), array('required' => true, 'defaulttime' => $midnight, 'optional' => ''));

		$radioarray=array();
		$radioarray[] =& $mform->createElement('radio', 'type_test', '', get_string('av1','report_uniceubasa'), 'av1');
		$radioarray[] =& $mform->createElement('radio', 'type_test', '', get_string('av2_ai','report_uniceubasa'), 'av2');
		$radioarray[] =& $mform->createElement('radio', 'type_test', '', get_string('avu_ai','report_uniceubasa'), 'avu');
		$mform->addGroup($radioarray, 'type_test', get_string('type_test','report_uniceubasa'), array(' '), false);

		if (isset($param->courses)) {
			foreach ($param->courses as $value)
				$mform->addElement('hidden', 'courses[]', $value);
			$mform->setType('courses[]', PARAM_INT);
		}

		$mform->addElement('text', 'subnet', get_string('iprestriction', 'report_uniceubasa'), 'size="30"');
		$mform->setType('subnet', PARAM_TEXT);
		$mform->setDefault('subnet', $param->subnet);

		$buttonarray=array();
		$buttonarray[] = &$mform->createElement('submit', 'proximo_submit', get_string('next'));
		$buttonarray[] = &$mform->createElement('cancel');
		$mform->addGroup($buttonarray, 'buttonar', '', array(''), false);
	}

	function validation($data, $files) {
		$errors = parent::validation($data, $files);

        if ($data['datestart'] != 0 && $data['dateend'] != 0 &&
                $data['dateend'] < $data['datestart']) {
            $errors['dateend'] = get_string('closebeforeopen', 'quiz');
        }

		return $errors;
	}
}

?>