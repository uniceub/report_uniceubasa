<?php

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * renderer.php
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

defined('MOODLE_INTERNAL') || die();

class report_uniceubasa_renderer extends plugin_renderer_base {

	/**
	 * This function builds the page "filter course"
	 *
	 * @param array $courses Courses to print
	 * @return string $output Code HTML to page
	 */
    public function filter_course_page($courses) {
    	$output = '';
		$output .= $this->heading(get_string('result_search','report_uniceubasa'), 3, '', '');
		if (!empty($courses))
			$output .= $this->print_table_course_search($courses);
		else
			$output .=  $this->error_text(get_string('noresult','report_uniceubasa'));
        return $output;
    }

	/**
	 * This function prints a table of courses for select courses
	 *
	 * @param array $course Courses to print
	 * @return string Code HTML of table
	 */
	function print_table_course_search($courses) {
		$head = array(html_writer::empty_tag('input', array('type' => 'checkbox', 'name' => 'selectall', 'id' => 'selectall')),get_string('name'), get_string('category'));
		$data = array();
		foreach ($courses as $course)
			$data[] = array(
				html_writer::empty_tag('input', array('type' => 'checkbox', 'name' => 'courses[]', 'value' => $course->id, 'class' => 'checkbox')),
				$course->fullname,
				$course->categoryfullname
			);
		$output = html_writer::start_tag('form', array('action' => '', 'method' => 'post', 'id' => 'filter_course'));
		$output .= $this->print_table($head, $data, array('2%', '49%', '49%'));
		$output .= html_writer::start_tag('div', array('class' => 'form-buttons'));
		$output .= html_writer::empty_tag('input', array('id' => 'continue_submit', 'name' => 'continue_submit', 'class' => 'form-submit', 'type' => 'submit', 'value' => get_string('continue'), 'disabled' => 'disabled'));
		$output .= html_writer::end_tag('div');
		$output .= html_writer::end_tag('form');
		return $output;
	}

	/**
	 * This function builds the page "password"
	 *
	 * @param array $courses Courses to print
	 * @return string $output Code HTML to page
	 */
    public function password_page($courses, $is_cohort_instructor = false) {
    	$output = '';
    	$output = $this->heading(get_string('activities'), 3, '', '');
    	if (!$is_cohort_instructor)
    		$output .= '<a id="button_active">Senhas ativas</a>';
		$output .= $this->heading(get_string('update','report_uniceubasa').' '.userdate(time()), 6, '', '');
    	foreach ($courses as $course) {
			$class = 'course';
			if ($this->course_check_active($course))
				$class .= ' no-active';
			$output .= html_writer::start_tag('div', array('class' => $class));
			$url = course_get_url($course->id);
			$coursefullname_link = html_writer::link($url, $course->fullname, array('class' => 'bold_inline'));
			$output .= $coursefullname_link;
			$url = new moodle_url('/course/index.php', array('categoryid' => $course->category));
			$categoryfullname_link = html_writer::link($url, $course->categoryfullname, array('class' => 'bold_inline'));
			$output .= $categoryfullname_link;
			$output .= $this->print_table_course_password($course, $is_cohort_instructor);
			$output .= html_writer::end_tag('div');
		}
		if (empty($courses))
			$output .=  $this->error_text(get_string('noresult','report_uniceubasa'));
        return $output;
    }

	/**
	 * This function prints a table of courses with password for each module
	 *
	 * @param array $course Courses to print
	 * @return string Code HTML of table
	 */
	function print_table_course_password($course, $is_cohort_instructor = false) {
		$output = '';
		$head = array(get_string('activity'), get_string('datestart','report_uniceubasa'), get_string('dateend','report_uniceubasa'), get_string('timemodified', 'report_uniceubasa'), get_string('iprestriction','report_uniceubasa'), get_string('password'), 'Modo Telão');
		$data = array();
		foreach ($course->cms as $cm)
			if (in_array($cm->idnumber, array('av1','av2','avu'))) {
				$datestart = $dateend = null;
				$condition = json_decode($cm->availability, true);
				if ($condition['c'])
					foreach ($condition['c'] as $key => $value)
						if ($value['type'] == 'date' && $value['d'] == '>=')
							$datestart = $value['t'];
						else if ($value['type'] == 'date' && $value['d'] == '<')
							$dateend = $value['t'];
				if ($datestart <= time() && $dateend >= time())
					$rowclasses[] = 'active';
				else {
					if ($is_cohort_instructor)
						continue;
					$rowclasses[] = 'no-active';
				}
				$url = new moodle_url('/mod/quiz/view.php', array('id'=>$cm->id));
				$activity_link = html_writer::link($url, $cm->activity->name);
				$timeopen 		= $cm->activity->timeopen 		? userdate($cm->activity->timeopen) 	: get_string('notdefined', 'report_uniceubasa');
				$timeclose 		= $cm->activity->timeclose 		? userdate($cm->activity->timeclose) 	: get_string('notdefined', 'report_uniceubasa');
				$timemodified 	= $cm->activity->timemodified 	? userdate($cm->activity->timemodified) : get_string('notdefined', 'report_uniceubasa');
				$subnet 		= $cm->activity->subnet 		? $cm->activity->subnet 				: get_string('notdefined', 'report_uniceubasa');
				$password 		= $cm->activity->password 		? $cm->activity->password 				: get_string('notdefined', 'report_uniceubasa');
                $fullpage = html_writer::link('?id='.$cm->id, html_writer::img($this->pix_url('e/fullpage'), 'Modo Telão'), array('target' => '_blank'));
				$data[] = array(
					$activity_link,
					$timeopen,
					$timeclose,
					$timemodified,
					$subnet,
					$password,
					$fullpage
				);
			}
		if ($data)
			$output .= $this->print_table($head, $data, array(), array('center','center','center','center', 'center','center', 'center'), $rowclasses);
		else
			$output .= $this->error_text(get_string('noresult','report_uniceubasa'));
		return $output;
	}

	/**
	 * This function
	 *
	 * @param array $course Courses to process
	 * @return boolean 
	 */
	function course_check_active($course) {
		foreach ($course->cms as $cm)
			if (in_array($cm->idnumber, array('av1','av2','avu'))) {
				$datestart = $dateend = null;
				$condition = json_decode($cm->availability, true);
				if ($condition['c'])
					foreach ($condition['c'] as $key => $value)
						if ($value['type'] == 'date' && $value['d'] == '>=')
							$datestart = $value['t'];
						else if ($value['type'] == 'date' && $value['d'] == '<')
							$dateend = $value['t'];
				if ($datestart <= time() && $dateend >= time())
					return false;
			}
		return true;
	}

	/**
	 * This function builds the page "time type"
	 *
	 * @param array $courses Courses to print
	 * @return string $output Code HTML to page
	 */
    public function time_type_page($courses) {
    	$output = '';
    	$output .= $this->heading(get_string('courses'), 3, '', ''); 
    	$output .= $this->print_table_time_type($courses);
        return $output;
    }

	/**
	 * This function prints a table of courses with modules checked
	 *
	 * @param array $courses Courses to print
	 * @return string Code HTML of table
	 */
	function print_table_time_type($courses) {
		$head = array(get_string('name'), get_string('category'), get_string('av1','report_uniceubasa'), get_string('av2','report_uniceubasa'), get_string('avu','report_uniceubasa'), get_string('ai','report_uniceubasa'));
		$data = array();
		foreach ($courses as $course)
			$data[] = array(
				$course->fullname,
				$course->categoryfullname,
				$this->course_check_activity($course, 'av1') ? "<img src='".$this->pix_url('t/approve')."' />" : "<img src='".$this->pix_url('t/delete')."' />",
				$this->course_check_activity($course, 'av2') ? "<img src='".$this->pix_url('t/approve')."' />" : "<img src='".$this->pix_url('t/delete')."' />",
				$this->course_check_activity($course, 'avu') ? "<img src='".$this->pix_url('t/approve')."' />" : "<img src='".$this->pix_url('t/delete')."' />",
				$this->course_check_activity($course, 'ai') ? "<img src='".$this->pix_url('t/approve')."' />" : "<img src='".$this->pix_url('t/delete')."' />"
			);
		return $this->print_table($head, $data, array('27%', '27%', '10%', '10%', '10%', '16%'), array('left','left','center','center','center','center'));
	}

	/**
	 * This function check if there is coursemodule by idnumber
	 *
	 * @param int $course Course to search
	 * @param string $idnumber Idnumber to check
	 * @return boolean
	 */
	function course_check_activity($course,$idnumber) {
		foreach ($course->cms as $cm)
			if ($cm->idnumber == $idnumber)
				return true;
		return false;
	}

	/**
	 * This function builds the page "report"
	 *
	 * @param array $param Parameters to build report page
	 * @return string $output Code HTML to report
	 */
	public function report_page($param) {
		$output = '';
		$output .= $this->heading(get_string('report'), 3, '', '');
		$output .= html_writer::start_tag('dl');
		$output .= html_writer::tag('dt', get_string('datestart', 'report_uniceubasa'));
		$output .= html_writer::tag('dd', userdate($param->datestart));
		$output .= html_writer::tag('dt', get_string('dateend', 'report_uniceubasa'));
		$output .= html_writer::tag('dd', userdate($param->dateend));
		$output .= html_writer::tag('dt', get_string('type_test','report_uniceubasa'));
		$output .= html_writer::tag('dd', get_string($param->type_test,'report_uniceubasa'));
		$output .= html_writer::tag('dt', get_string('password'));
		$output .= html_writer::tag('dd', $param->password);
		$output .= html_writer::tag('dt', get_string('iprestriction', 'report_uniceubasa'));
		$output .= html_writer::tag('dd', $param->subnet ? $param->subnet : get_string('notdefined', 'report_uniceubasa'));
		$output .= html_writer::tag('dt', get_string('user'));
		$output .= html_writer::tag('dd', fullname($param->user));
		$output .= html_writer::tag('dt', get_string('update','report_uniceubasa'));
		$output .= html_writer::tag('dd', userdate($param->time));
		$output .= html_writer::end_tag('dl');
		$output .= $this->print_table_course($param->courses);
		$output .= html_writer::start_tag('form', array('action' => '', 'method' => 'post', 'id' => 'report_view'));
		$output .= html_writer::empty_tag('input', array('name' => 'back_submit', 'class' => 'form-submit', 'type' => 'submit', 'value' => get_string('backto','', get_string('pluginname', 'report_uniceubasa'))));
		$output .= html_writer::end_tag('form');
        return $output;
    }

	/**
	 * This function prints a table of courses with name and name category 
	 *
	 * @param array $courses Courses to print
	 * @return string Code HTML of table
	 */
	function print_table_course($courses) {
		if (empty($courses))
			return $this->error_text(get_string('noresult','report_uniceubasa'));
		$head = array(get_string('name'), get_string('category'));
		$data = array();
		foreach ($courses as $course)
			$data[] = array($course->fullname,$course->categoryfullname);
		return $this->print_table($head, $data);
	}

	/**
	 * This function is configured with the parameters patterns to print a table, stating only the header and data
	 *
	 * @param array $head The header to print in the table
	 * @param array $data The data to print in the table
	 * @param array $size The size cell to print in the table
	 * @param array $align The align cell to print in the table
	 * @return string Code HTML of table
	 */
	function print_table($head, $data, $size = array(), $align = array(), $rowclasses = array()) {
		$table = new html_table();
		$table->class = 'generaltable';
		$table->width = '100%';
		$table->size = $size;
		$table->align = $align;
		$table->rowclasses = $rowclasses;
		$table->head = array();
		foreach ($head as $value)
			$table->head[] = $value;
		$table->data = array();
		foreach ($data as $value)
			$table->data[] = $value;
		return html_writer::table($table);
	}

}