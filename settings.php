<?php

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * settings.php
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

defined('MOODLE_INTERNAL') || die;

$ADMIN->add('reports', new admin_externalpage('uniceubasa', get_string('pluginname', 'report_uniceubasa'), "$CFG->wwwroot/report/uniceubasa/index.php"));

if ($ADMIN->fulltree) {
    $options = array('5' => '5 minutos', '10' => '10 minutos', '15' => '15 minutos');
    $settings->add(new admin_setting_configselect('uniceubasa_interval', get_string('settings_interval', 'report_uniceubasa'), '', '5', $options));
    $options = array('0' => 'Somente números [0123456789]', '1' => 'Letras e números [ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789]');
    $settings->add(new admin_setting_configselect('uniceubasa_password', get_string('password'), '', '0', $options));
    $settings->add(new admin_setting_configtext('uniceubasa_subnet', get_string('iprestriction', 'report_uniceubasa'), '', ''));
}
