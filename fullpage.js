

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * fullpage.js
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

$(document).ready(function() {
    $('#telao_block').css('margin-left', $('#telao_block').width() / 2 * -1);
    setTimeout(function() {
        window.location.reload();
    },20000);
});