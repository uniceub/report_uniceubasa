<?php

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * index.php
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once("index_form.php");
require_once($CFG->dirroot.'/cohort/lib.php');

require_login();

if (!$DB->record_exists('cohort', array('idnumber' => 'aplicadores'))) {
    $cohort = new stdClass();
    $cohort->name = 'Aplicadores';
    $cohort->idnumber = 'aplicadores';
    $cohort->description = 'Aplicadores de Provas';
    $cohort->descriptionformat = FORMAT_HTML;
    $cohort->component = '';
    $cohort->timecreated = time();
    $cohort->timemodified = $cohort->timecreated;
    $cohort->contextid = 1;
    $id = cohort_add_cohort($cohort);
}
$cohort_instructor = $DB->get_record('cohort', array('idnumber' => 'aplicadores'), '*', MUST_EXIST);
if (!is_siteadmin() && !cohort_is_member($cohort_instructor->id, $USER->id))
	redirect(new moodle_url('/index.php'), get_string('nopermissions', 'report_uniceubasa'));

if (is_siteadmin())
	admin_externalpage_setup('uniceubasa', '', null, '', array('pagelayout'=>'report'));
else {
	require_once('lib.php');
	$PAGE->set_context(null);
	$PAGE->set_url('/report/uniceubasa/index.php', null);
	$PAGE->set_pagelayout('report');
	$PAGE->set_title($SITE->fullname.': '.get_string('pluginname','report_uniceubasa'));
	$PAGE->set_heading($SITE->fullname);
}

$PAGE->requires->jquery();
$PAGE->requires->js('/report/uniceubasa/javascript.js');

$output = $PAGE->get_renderer('report_uniceubasa');

$param = new object();
$param->name = optional_param('name', NULL,PARAM_TEXT);
$param->category = optional_param('category', NULL,PARAM_INT);
$param->back = (bool) optional_param('back_submit', NULL,PARAM_TEXT);
$param->continue = (bool) optional_param('continue_submit', NULL,PARAM_TEXT);
$param->courses = optional_param_array('courses', NULL,PARAM_INT);
$param->subnet = optional_param('subnet', NULL,PARAM_TEXT);
$param->id = optional_param('id', NULL, PARAM_INT);

if ($cm = $DB->get_record('course_modules', array('id' => $param->id))) {
    $quiz = $DB->get_record('quiz', array('id' => $cm->instance));
    if ($quiz->timeopen <= time() && $quiz->timeclose >= time()) {
        $PAGE->set_pagelayout('embedded');
        $PAGE->requires->js('/report/uniceubasa/fullpage.js');
        echo $OUTPUT->header();
        echo html_writer::start_div('', array('id' => 'telao_block', 'style' => 'position:absolute;top:50%;left:50%;margin-left:-100px;margin-top:-100px;text-align:center;'));
        echo html_writer::tag('h2', mb_strtoupper(get_string('password')), array('id'=>'telao_title', 'style' => 'font-size:60px;display:inline-block;'));
        echo html_writer::tag('h1', $quiz->password, array('id'=>'telao_password', 'style' => 'font-size:80px;  padding: 20px 0px;'));
        echo html_writer::tag('h4', mb_strtoupper(get_course($cm->course)->fullname.' - '.$quiz->name, 'UTF-8'), array('id'=>'telao_coursename', 'style' => 'font-size:20px;display:inline-block;'));
        echo html_writer::end_div();
        echo $OUTPUT->footer();
        die;
    } else {
        redirect(new moodle_url('/report/uniceubasa/index.php'), get_string('nopermissions', 'report_uniceubasa'));
    }
}

$fc_form = new filter_course_form();
$tt_form = new time_type_form();
$ru_lib = new report_uniceubasa_lib();

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname','report_uniceubasa'));
echo $OUTPUT->box_start();

if (is_siteadmin()) {
	if ($tt_data = $tt_form->get_data()) {
		$result = $ru_lib->process_courses($tt_data, $param->courses);
		echo $output->report_page($result);
	} else if ($param->courses && !$tt_form->is_cancelled()) {
		$tt_form->display();
		$courses = $ru_lib->time_type($param->courses);
		echo $output->time_type_page($courses);
	} else {
		$fc_form->display();
		if ($fc_data = $fc_form->get_data()) {
			if (isset($fc_data->search_submit)) {
				$courses = $ru_lib->filter_course($param->name, $param->category);
				echo $output->filter_course_page($courses);
			} else if (isset($fc_data->password_submit)) {
				$courses = $ru_lib->password_courses($param->name, $param->category);
				echo $output->password_page($courses);
			}
		}
	}
} else {
	$courses = $ru_lib->password_courses('', 0, true);
	echo $output->password_page($courses, true);
}

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
