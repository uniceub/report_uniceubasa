<?php

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * lib.php
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

class report_uniceubasa_lib {

	/**
	 * This function filters the courses with the reported parameters
	 *
	 * @param string $coursename Course name to search
	 * @param int $categoryid Category id to search
	 * @return array $courses Courses with category name
	 */
	function filter_course($coursename, $categoryid) {
		$courses = $this->courses_search($coursename, $categoryid);
		return $courses;
	}

	/**
	 * This function return the modules by courses with the reported parameters
	 *
	 * @param string $coursename Course name to search
	 * @param int $categoryid Category id to search
	 * @return array $courses Courses with category name and modules
	 */
	function password_courses($name, $category, $is_cohort_instructor = false) {
		$courses = $this->courses_search($name, $category, $is_cohort_instructor);
		$courses = $this->get_course_activities_by_courses($courses);
		return $courses;
	}

	/**
	 * This function return the modules by coursesid
	 *
	 * @param array $coursesid Coursesid to return
	 * @return array $courses Courses with category name and modules
	 */
	function time_type($coursesid) {
		$courses = $this->get_course_activities($coursesid);
		return $courses;
	}

	/**
	 * This function processes the modules based on the data reported in the Form
	 *
	 * @param object $tt_data Data reported on Form
	 * @param array $coursesid Coursesid to process
	 * @return array $courses Parameters of modification
	 */
	function process_courses($tt_data,$coursesid){
		global $CFG, $DB, $USER;
		$param = new object();
		$param->datestart = $tt_data->datestart;
		$param->dateend = $tt_data->dateend;
		$param->type_test = $tt_data->type_test;
		$param->password = $this->password();
		if (trim($tt_data->subnet) === false) {
			$param->subnet = $tt_data->subnet;
		} else {
			$param->subnet = $CFG->uniceubasa_subnet;
		}
		$param->user = $USER;
		$param->time = time();
		$param->courses = array();
		$param->cron = false;
		$courses = $this->get_course_activities($coursesid);
		foreach ($courses as $course)
			foreach ($course->cms as $cm)
				if ($cm->idnumber == $param->type_test) {
					$ai = null;
					$condition = array(
						'op' => '&',
						'c' => array(
							array('type' => 'date', 'd' => '>=', 't' => (int) $param->datestart),
							array('type' => 'date', 'd' => '<', 't' => (int) $param->dateend)
							),
						'showc' => array(true, true)
						);
					if (in_array($cm->idnumber, array('av2','avu'))) {
						$ai = $DB->get_record('course_modules', array('course' => $cm->course, 'idnumber' => 'ai'));
						if ($ai) {
							$condition['c'][] = array('type' => 'completion', 'cm' => (int) $ai->id, 'e' => (int) 1);
							$condition['showc'][] = true;
						}
					}
					if (!is_null($cm->availability)) {
						$conditions = json_decode($cm->availability, true);
						$datestart = false;
						$dateend = false;
						if (is_null($conditions['c'])) {
							foreach ($conditions['c'] as $key => $value) {
								if ($value['type'] != 'date') {
									if ($value['type'] != 'completion' && $value['cm'] != $ai->id) {
										$condition['c'] = $value;
										$condition['showc'] = $conditions['showc'][$key];
									}
								}
							}
						}
					}
					$DB->set_field('course_modules', 'availability', json_encode($condition), array('id' => $cm->id));
					$DB->set_field('quiz', 'timeopen', $param->datestart, array('id' => $cm->activity->id));
					$DB->set_field('quiz', 'timeclose', $param->dateend, array('id' => $cm->activity->id));
					$DB->set_field('quiz', 'password', $param->password, array('id' => $cm->activity->id));
					$DB->set_field('quiz', 'subnet', $param->subnet, array('id' => $cm->activity->id));
					$DB->set_field('quiz', 'timemodified', time(), array('id' => $cm->activity->id));
					rebuild_course_cache($course->id);
					$param->courses[] = $course;
				}
		$this->send_report($param);
		return $param;
	}

	/**
	 * This function search the courses by the parameters reported
	 *
	 * @param string $coursename Course name to search
	 * @param int $categoryid Category id to search
	 * @return array $courses Courses with category name
	 */
	function courses_search($coursename, $categoryid, $is_cohort_instructor = false) {
		global $DB;
		$courses = array();
		if ($is_cohort_instructor) {
			$select = "idnumber in ('av1','av2','avu')";
			$cms = $DB->get_records_select('course_modules', $select);
			foreach ($cms as $cm) {
				$datestart = $dateend = null;
				$condition = json_decode($cm->availability, true);
				if ($condition['c']) {
					foreach ($condition['c'] as $key => $value)
						if ($value['type'] == 'date' && $value['d'] == '>=')
							$datestart = $value['t'];
						else if ($value['type'] == 'date' && $value['d'] == '<')
							$dateend = $value['t'];
				}
				if ($datestart <= time() && $dateend >= time())
					$courses[] = get_course($cm->course);
			}
		} else {
			if ($categoryid) {
				$category_in = '';
				$subcategories = coursecat::get($categoryid)->get_children();
				foreach ($subcategories as $subcategory)
					$category_in .= ','.$subcategory->id;
				$wsql = "AND category in ($categoryid$category_in) ";
				$courses = $DB->get_records_select('course',"category not in ('0') AND fullname LIKE '%$coursename%' AND visible = 1 $wsql ORDER BY fullname ASC");
			}
			else
				$courses = $DB->get_records_select('course',"category not in ('0') AND fullname LIKE '%$coursename%' AND visible = 1 ORDER BY fullname ASC");
		}
		$categories = coursecat::make_categories_list();
		foreach ($courses as $course)
			$course->categoryfullname = $categories[$course->category];
		return $courses;
	}

	/**
	 * This function return courses with modules
	 *
	 * @param array $courses Courses to process
	 * @return array $courses Course with modules
	 */
	function get_course_activities_by_courses($courses) {
		global $DB;
		foreach ($courses as $course){
			$cms = $DB->get_records('course_modules',array('course'=>$course->id));
			foreach ($cms as $cm)
				if (!is_null($cm->idnumber)) {
					$cm->activity = $this->get_activity($cm);
					$course->cms[$cm->idnumber] = $cm;
				}
		}
		return $courses;
	}

	/**
	 * This function return the modules by coursesid
	 *
	 * @param int $coursesid Courses id to process
	 * @return array $courses Course with modules
	 */
	function get_course_activities($coursesid) {
		global $DB;
		$categories = coursecat::make_categories_list();
		foreach ($coursesid as $courseid){
			$course = get_course($courseid);
			$course->categoryfullname = $categories[$course->category];
			$cms = $DB->get_records('course_modules',array('course'=>$course->id));
			foreach ($cms as $cm)
				if (!is_null($cm->idnumber)) {
					$cm->activity = $this->get_activity($cm);
					$course->cms[$cm->idnumber] = $cm;
				}
			$courses[] = $course;
		}
		return $courses;
	}

	/**
	 * This function returns a module by coursemodule
	 *
	 * @param object $cm Coursemodule to search
	 * @return object $module Module
	 */
	function get_activity($cm) {
		global $DB;
		$result = $DB->get_record($this->get_mod_name($cm->module),array('id'=>$cm->instance));
		return $result ? $result : null;
	}

	/**
	 * This function returns the name of the module by modid
	 *
	 * @param int $modid Modid to search
	 * @return string $name Module name
	 */
	function get_mod_name($modid){
		global $DB;
		$result = $DB->get_record('modules',array('id'=>$modid));
		return $result->name;
	}

	/**
	 * This function generates new password with six chars between numbers and consonants
	 *
	 * @return string $password 
	 */
	static function password(){
		global $CFG;
		$option[0] = '0123456789';
		$option[1] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
		$characters = $option[$CFG->uniceubasa_password];
		$password = '';
		$last = '';
		for ($i = 0; strlen($password) < 6; $i++){
			$char = $characters[rand(0, strlen($characters)-1)];
			if ($last != $char)
				$password .= $char;
			$last = $char;
		}
		return $password;
	}

	/**
	 * This function builds code HTML for report
	 *
	 * @param object $param Parameters to construct the message
	 * @return string $output Code HTML for send message
	 */
	function report_mail($param){
		$style_font = 'font-family:arial';
		$style_bold = 'font-weight:bold';
		$style_minwidth = 'min-width:400px;border-collapse: collapse;';
		$style_cell_table = 'padding:5px; border: 1px solid black;';
		$output = '';
		
		$output .= html_writer::start_tag('div', array('style' => $style_font));

		$output .= html_writer::tag('h3', "Dados da alteração");
		
		$output .= html_writer::start_tag('table', array('style' => ''));
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('td').get_string('datestart', 'report_uniceubasa').html_writer::end_tag('td');
		$output .= html_writer::start_tag('td', array('style' => $style_bold)).userdate($param->datestart).html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('td').get_string('dateend', 'report_uniceubasa').html_writer::end_tag('td');
		$output .= 	html_writer::start_tag('td', array('style' => $style_bold)).userdate($param->dateend).html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::start_tag('tr');
		$output .= 	html_writer::start_tag('td').get_string('type_test','report_uniceubasa').html_writer::end_tag('td');
		$output .= html_writer::start_tag('td', array('style' => $style_bold)).get_string($param->type_test,'report_uniceubasa');html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::start_tag('tr');
		$output .= 	html_writer::start_tag('td').get_string('password').html_writer::end_tag('td');
		$output .= html_writer::start_tag('td', array('style' => $style_bold)).$param->password.html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('td').get_string('iprestriction', 'report_uniceubasa').html_writer::end_tag('td');
		$output .= html_writer::start_tag('td', array('style' => $style_bold)).$param->subnet.html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('td').get_string('user').html_writer::end_tag('td');
		$output .= 	html_writer::start_tag('td', array('style' => $style_bold)).fullname($param->user).html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('td').get_string('update','report_uniceubasa').html_writer::end_tag('td');
		$output .= 	html_writer::start_tag('td', array('style' => $style_bold)).userdate($param->time).html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::end_tag('table');

		$output .= html_writer::tag('h3', get_string('course'));

		$output .= html_writer::start_tag('table', array('style' => $style_minwidth));
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('th', array('style' => $style_cell_table)).get_string('name').html_writer::end_tag('th');
		$output .= html_writer::start_tag('th', array('style' => $style_cell_table)).get_string('category').html_writer::end_tag('th');
		$output .= html_writer::end_tag('tr');
		foreach ($param->courses as $course) {
			$output .= html_writer::start_tag('tr');
			$output .= html_writer::start_tag('td', array('style' => $style_cell_table)).$course->fullname.html_writer::end_tag('td');
			$output .= html_writer::start_tag('td', array('style' => $style_cell_table)).$course->categoryfullname.html_writer::end_tag('td');
			$output .= html_writer::end_tag('tr');
		}
		$output .= html_writer::end_tag('table');

		$output .= html_writer::end_tag('div');
		
		return $output;
	}

	/**
	 * This function builds code HTML for cron report
	 *
	 * @param object $param Parameters to construct the message
	 * @return string $output Code HTML for send message
	 */
	function report_cron($param){
		$style_font = 'font-family:arial';
		$style_bold = 'font-weight:bold';
		$style_minwidth = 'min-width:400px;border-collapse: collapse;';
		$style_cell_table = 'padding:5px; border: 1px solid black;';
		$style_text_right = 'text-aling:right;';
		$output = '';
		
		$output .= html_writer::start_tag('div', array('style' => $style_font));

		$output .= html_writer::tag('h3', "Dados da alteração");
		
		$output .= html_writer::start_tag('table', array('style' => ''));
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('td').get_string('password').html_writer::end_tag('td');
		$output .= html_writer::start_tag('td', array('style' => $style_bold)).$param->password.html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('td').get_string('iprestriction', 'report_uniceubasa').html_writer::end_tag('td');
		$output .= html_writer::start_tag('td', array('style' => $style_bold)).$param->subnet.html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('td').get_string('update','report_uniceubasa').html_writer::end_tag('td');
		$output .= html_writer::start_tag('td', array()).userdate($param->time).html_writer::end_tag('td');
		$output .= html_writer::end_tag('tr');
		$output .= html_writer::end_tag('table');

		$output .= html_writer::tag('h3', get_string('course'));

		$output .= html_writer::start_tag('table', array('style' => $style_minwidth));
		$output .= html_writer::start_tag('tr');
		$output .= html_writer::start_tag('th', array('style' => $style_cell_table)).get_string('category').html_writer::end_tag('th');
		$output .= html_writer::start_tag('th', array('style' => $style_cell_table)).get_string('course').html_writer::end_tag('th');
		$output .= html_writer::start_tag('th', array('style' => $style_cell_table)).get_string('activity').html_writer::end_tag('th');
		$output .= html_writer::start_tag('th', array('style' => $style_cell_table)).get_string('datestart','report_uniceubasa').html_writer::end_tag('th');
		$output .= html_writer::start_tag('th', array('style' => $style_cell_table)).get_string('dateend','report_uniceubasa').html_writer::end_tag('th');
		$output .= html_writer::end_tag('tr');
		foreach ($param->cms as $category => $courses)
			foreach ($courses as $course => $cms)
				foreach ($cms as $cm) {
					$output .= html_writer::start_tag('tr');
					$output .= html_writer::start_tag('td', array('style' => $style_cell_table)).
								$category.
								html_writer::end_tag('td');
					$output .= html_writer::start_tag('td', array('style' => $style_cell_table)).
								$course.
								html_writer::end_tag('td');
					$output .= html_writer::start_tag('td', array('style' => $style_cell_table)).
								$cm->activity->name.
								html_writer::end_tag('td');
					$output .= html_writer::start_tag('td', array('style' => $style_cell_table.$style_text_right)).
								userdate($cm->activity->timeopen).
								html_writer::end_tag('td');
					$output .= html_writer::start_tag('td', array('style' => $style_cell_table.$style_text_right)).
								userdate($cm->activity->timeclose).
								html_writer::end_tag('td');
					$output .= html_writer::end_tag('tr');
				}
		$output .= html_writer::end_tag('table');

		$output .= html_writer::end_tag('div');
		
		return $output;
	}

	/**
	 * This function sends message using the Moodle API
	 *
	 * @param object $param Parameters to construct the message
	 */
	function send_report($param){
		$eventdata = new stdClass();
		$eventdata->name 				= 'report';
		$eventdata->component 			= 'report_uniceubasa';
		$eventdata->userfrom 			= get_admin();
		$eventdata->subject 			= get_string('pluginname', 'report_uniceubasa');
		$eventdata->fullmessage 		= '';
		$eventdata->fullmessageformat 	= FORMAT_PLAIN;
		if ($param->cron)
			$eventdata->fullmessagehtml = $this->report_cron($param);
		else if (isset($param->courses)) {
			if (empty($param->courses))
				return;
			$eventdata->fullmessagehtml = $this->report_mail($param);
		}
		$eventdata->smallmessage = '';
		foreach (get_admins() as $user) {
			if (in_array($user->id, array(1861,1645,4558))) {
				$eventdata->userto = $user;
				message_send($eventdata);
			}
		}
	}

}

	/**
	 * This function is run in cron processing
	 */
	function uniceubasa_cron() {
		global $CFG, $DB, $USER;

		$trace = new text_progress_trace();
		$trace->output('');
		$trace->output('');
		$trace->output('Starting report uniceubasa');
		$trace->output('');

		require_once($CFG->dirroot.'/course/lib.php');
		require_once($CFG->libdir.'/coursecatlib.php');

		$param = new object();
		$param->password = report_uniceubasa_lib::password();
		$param->cms = array();
		$param->cron = true;
		$param->interval = 60 * ((int) $CFG->uniceubasa_interval) - 40;
		$param->time = time();

		$select = "idnumber in ('av1','av2','avu')";
		$cms = $DB->get_records_select('course_modules',$select);

		$categories = coursecat::make_categories_list();

		foreach ($cms as $cm) {
			$datestart = $dateend = null;
			$condition = json_decode($cm->availability, true);
			if ($condition['c'])
				foreach ($condition['c'] as $key => $value)
					if ($value['type'] == 'date' && $value['d'] == '>=')
						$datestart = $value['t']-900;
					else if ($value['type'] == 'date' && $value['d'] == '<')
						$dateend = $value['t']+36000;
			if ($datestart <= time() && $dateend >= time()) {
				$table = $DB->get_record('modules',array('id'=>$cm->module));
				$cm->activity = $DB->get_record($table->name,array('id'=>$cm->instance));
				if ($cm->activity->timemodified + $param->interval <= time() &&
					$cm->activity->timeopen 	+ $param->interval <= time()) {
					$DB->set_field($table->name, 'password', $param->password, array('id' => $cm->activity->id));
					$DB->set_field($table->name, 'timemodified', time(), array('id' => $cm->activity->id));
					$course = get_course($cm->course);
					$param->cms[$categories[$course->category]][$course->fullname][] = $cm;
				}
				if ($cm->activity->timeopen-600 <= time() && time() <= $cm->activity->timeclose) {
				    if ($section = $DB->get_record("course_sections", array("course"=>$cm->course, "section"=>2)))
				        $DB->set_field("course_sections", "visible", "0", array("id"=>$section->id));
				    if ($section = $DB->get_record("course_sections", array("course"=>$cm->course, "section"=>3)))
				        $DB->set_field("course_sections", "visible", "0", array("id"=>$section->id));
				    if ($section = $DB->get_record("course_sections", array("course"=>$cm->course, "section"=>5)))
				        $DB->set_field("course_sections", "visible", "0", array("id"=>$section->id));
				    rebuild_course_cache($cm->course, true);
					$trace->output('Course '.$cm->course.' - Topics 2, 3 and 5 hidden');
				} else {
				    if ($section = $DB->get_record("course_sections", array("course"=>$cm->course, "section"=>2)))
				        $DB->set_field("course_sections", "visible", "1", array("id"=>$section->id));
				    if ($section = $DB->get_record("course_sections", array("course"=>$cm->course, "section"=>3)))
				        $DB->set_field("course_sections", "visible", "1", array("id"=>$section->id));
				    if ($section = $DB->get_record("course_sections", array("course"=>$cm->course, "section"=>5)))
				        $DB->set_field("course_sections", "visible", "1", array("id"=>$section->id));
				    rebuild_course_cache($cm->course, true);
					$trace->output('Course '.$cm->course.' - Topics 2, 3 and 5 visible');
				}
			}
		}

		if (!empty($param->cms)) {
			$trace->output('Senha: '.$param->password);
			$trace->output('');
			foreach ($param->cms as $category => $courses)
				foreach ($courses as $course => $cms)
					foreach ($cms as $cm)
						$trace->output($category." - ".$course." - ".$cm->activity->name);
			// send email with new password
			// report_uniceubasa_lib::send_report($param);
		} else
			$trace->output('No modified activity');

		$trace->output('');
		$trace->output('Finished report uniceubasa');
		$trace->output('');
		$trace->output('');
	}
