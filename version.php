<?php

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * version.php
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2015061600;
$plugin->requires  = 2014111000;
$plugin->component = 'report_uniceubasa';
