

/**
 * UniCEUB
 * ASA - Automatização de Senhas para Avaliações
 * javascript.js
 *
 * @package    report
 * @subpackage uniceubasa
 * @author     Michael Meneses <michael.souza@uniceub.br>
 * @copyright  2014 UniCEUB
 */

$(document).ready(function() {

    var selectall = $("#selectall");
    var checkboxes = $(".checkbox");
    var continueSubmit = $("#continue_submit");

    selectall.click(function(event) {
        if(this.checked) {
            $('.checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $('.checkbox').each(function() {
                this.checked = false;
            });
        }
    });

    checkboxes.click(function() {
        continueSubmit.attr("disabled", !checkboxes.is(":checked"));
        selectall.attr("checked", !checkboxes.is(":checked"));
        if(checkboxes.filter(':not(:checked)').length === 0){
            selectall.prop("checked", true);
        } else {
            selectall.prop("checked", false);
        }
    });

    selectall.click(function() {
        continueSubmit.attr("disabled", !checkboxes.is(":checked"));
    });

    $('input[name=proximo_submit]').attr("disabled", "disabled");
    if($('input[name=type_test]').is(':checked')){
        $('input[name=proximo_submit]').removeAttr("disabled");
    }
    $('input[name=type_test]').click(function(){
        $('input[name=proximo_submit]').removeAttr("disabled");
    });

    $('#button_active').click(function(){
        var no_active = $('.no-active');
        no_active.toggle();
        if (no_active.is(':visible'))
            $('#button_active').text("Mostrar ativas");
        else
            $('#button_active').text("Mostrar todas");
    });

});